package cn.hacz.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * project - ETC发票系统
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/3/25 时间:10:28
 * @JDK 1.8
 * @Description 功能模块：
 */
@SpringBootApplication
public class Learning0902Application {
    public static void main(String[] args) {
        SpringApplication.run(Learning0902Application.class, args);
    }
}
