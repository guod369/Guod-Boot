package cn.hacz.edu.controller;

import cn.hacz.edu.entity.UserEntity;
import cn.hacz.edu.vo.UserVo;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * project -
 * description -
 *
 * @author：guod ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：10:23
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@RestController
public class ClientController {
    RestTemplate restTemplate = new RestTemplate();

    /**
     * 功能：调用路径做为参数
     *
     * @return对象实体
     */
    @GetMapping(value = "/clientGet")
    public UserEntity clientGet() {
        UserEntity forObject = restTemplate.getForObject("http://localhost:6666/getFullVariable/{id}", UserEntity.class, 11);
        System.out.println(forObject.getId());
        return forObject;
    }

    /**
     * 功能：form表单参数
     *
     * @return对象实体
     */
    @GetMapping(value = "/clientPostObj")
    public UserEntity clientPostObj() {
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("id", "22");
        UserEntity userEntity = restTemplate.postForObject("http://localhost:6666/postFullObject", map, UserEntity.class);
        System.out.println(userEntity);
        return userEntity;
    }

    /**
     * 功能：JSON参数
     *
     * @return对象实体
     */
    @GetMapping(value = "/clientPostJson")
    public UserEntity clientPostJson() {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1);
        userEntity.setName("guod");
        UserEntity userEntity1 = restTemplate.postForObject("http://localhost:6666/postFullJson", userEntity, UserEntity.class);
        System.out.println(userEntity1);
        return userEntity;
    }

    @GetMapping(value = "/test01")
    public UserVo test01() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", 1);
        System.out.println(map);
        UserVo forObject = restTemplate.getForObject("http://localhost:6666/test", UserVo.class, map);
        return null;
    }
}
