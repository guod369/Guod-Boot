package cn.hacz.edu.controller;

import cn.hacz.edu.entity.UserEntity;
import cn.hacz.edu.vo.UserVo;
import org.springframework.web.bind.annotation.*;

/**
 * project -
 * description -
 *
 * @author：guod ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：9:52
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@RestController
public class ServiceController {
    /**
     * 功能：不带参数的rest full
     *
     * @return
     */
    @GetMapping(value = "/getFull")
    public String getFull() {
        return "getFull";
    }

    @PostMapping(value = "/postFull")
    public String postFull() {
        return "postFull";
    }

    @PutMapping(value = "/putFull")
    public String putFull(UserEntity userEntity) {
        return "putFull" + userEntity.getName();
    }

    @DeleteMapping(value = "/deleteFull")
    public String deleteFull() {
        return "deleteFull";
    }

    /**
     * 功能：带参数的rest full
     *
     * @param id
     * @return
     */
    @GetMapping(value = "/getFullVariable/{id}")
    public UserEntity getFullVariable(@PathVariable Integer id) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(id);
        return userEntity;
    }

    @PostMapping(value = "/postFullVariable/{id}")
    public String postFullVariable(@PathVariable(value = "id") Integer id) {
        return "postFullVariable" + id;
    }

    @PutMapping(value = "/putFullVariable/{id}")
    public String putFullVariable(@PathVariable Integer id) {
        return "putFullVariable" + id;
    }

    @DeleteMapping(value = "/deleteFull/{id}")
    public String deleteFullVariable(@PathVariable Integer id) {
        return "deleteFull" + id;
    }


    /**
     * 功能：通过对象的方式的rest full
     *
     * @return
     */
    @GetMapping(value = "/getFullObject")
    public UserEntity getFullObject(UserEntity userEntity) {
        UserEntity userEntityObj = new UserEntity();
        return userEntity;
    }

    @PostMapping(value = "/postFullObject")
    public UserEntity postFullObject(UserEntity userEntity) {
        UserEntity userEntityObj = new UserEntity();
        userEntityObj.setId(userEntity.getId());
        return userEntityObj;
    }

    @PutMapping(value = "/putFullObject")
    public String putFullObject(UserEntity userEntity) {
        return "putFullObject" + userEntity.getName();
    }

    @DeleteMapping(value = "/deleteFullObject")
    public String deleteFullObject() {
        return "deleteFullObject";
    }

    /**
     * 功能：通过json对象的方式的rest full
     *
     * @return
     */
    @GetMapping(value = "/getFullJson")
    public String getFullJson(@RequestBody UserEntity userEntity) {
        return "getFullJson" + userEntity.getName();
    }

    @PostMapping(value = "/postFullJson")
    public UserEntity postFullJson(@RequestBody UserEntity userEntity) {
        UserEntity userEntityJson = new UserEntity();
        userEntityJson.setId(userEntity.getId());
        return userEntityJson;
    }

    @PutMapping(value = "/putFullJson")
    public String putFullJson(@RequestBody UserEntity userEntity) {
        return "putFullJson" + userEntity.getName();
    }

    @DeleteMapping(value = "/deleteFullJson")
    public String deleteFullJson(@RequestBody UserEntity userEntity) {
        return "deleteFullJson";
    }

    @GetMapping(value = "/test")
    public String test(@RequestBody UserVo userVo) {
        System.out.println(userVo.getId());
        return null;
    }
}
