package cn.hacz.edu.fastjson;

import cn.hacz.edu.entity.UserEntity;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/8
 * Time：15:20
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
public class FastJson05MainTest {
    UserEntity info = new UserEntity("zhangsan", 24);

    @Test
    public void FastJson03Test01() {
        //UserInfo的序列化,此处使用的是javabean中的toString方法
        System.out.println(info);
        //将对象转换为JSON字符串
        String strJson = JSON.toJSONString(info);
        System.out.println("JSON=" + strJson);
        //UserInfo的反序列化
        UserEntity userInfo = JSON.parseObject(strJson, UserEntity.class);
        //此处使用的是javabean中的toString方法
        System.out.println(info + "+name:" + userInfo.getName() + "+age:" + userInfo.getAge());
    }

    @Test
    public void FastJson03Test02() {
        //List<UserEntity>序列化
        List<UserEntity> userInfoList = new ArrayList<>();
        UserEntity info1 = new UserEntity("lisi", 30);
        userInfoList.add(info);
        userInfoList.add(info1);
        //此处使用的是javabean中的toString方法
        System.out.println(userInfoList);
        String listStrJson = JSON.toJSONString(userInfoList);
        System.out.println(listStrJson);
        //List<UserEntity>反序列化
        List<UserEntity> userInfoList1 = JSON.parseArray(listStrJson, UserEntity.class);
        //此处使用的是javabean中的toString方法
        System.out.println(userInfoList1);
    }

    @Test
    public void FastJson03Test03() {
        //List<Map<String, Object>>序列化
        List<Map<String, Object>> list = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("zhangsan", 24);
        map.put("lisi", 30);
        list.add(map);
        System.out.println(list);
        String listString = JSON.toJSONString(list);
        System.out.println(listString);
        //List<Map<String, Object>>反序列化
        List<Map<String, Object>> list1 = JSON.parseObject(listString, new TypeReference<List<Map<String, Object>>>() {
        });
        System.out.println(list1);

    }
}
