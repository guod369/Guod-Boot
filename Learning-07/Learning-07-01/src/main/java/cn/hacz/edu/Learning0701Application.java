package cn.hacz.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/8
 * Time：14:48
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@SpringBootApplication
public class Learning0701Application {
    public static void main(String[] args) {
        SpringApplication.run(Learning0701Application.class, args);
    }
}
