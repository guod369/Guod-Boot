package cn.hacz.edu.fastjson;

import cn.hacz.edu.entity.PersonEntity;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/8
 * Time：16:06
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
public class FastJson02MainTest {
    @Test
    public void fastJson02MainTest01() {
        Map<String, Object> map = new HashMap<>();
        List<PersonEntity> personEntities = new ArrayList<>();
        personEntities.add(new PersonEntity("guod", 22, "18838177689"));
        personEntities.add(new PersonEntity("guod", 22, "18838177689"));
        personEntities.add(new PersonEntity("sun", 22, "18838177689"));
        map.put("persons", personEntities);
        // 序列化
        String s = JSON.toJSONString(map);
        System.out.println(s);

        JSONObject jsonObject = JSON.parseObject(s);
        JSONArray persons = jsonObject.getJSONArray("persons");
        for (int i = 0; i < persons.size(); i++) {
            JSONObject jsonObject1 = persons.getJSONObject(i);
            String name = jsonObject1.getString("name");
            System.out.println(name);
        }

    }
}
