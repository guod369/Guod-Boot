package cn.hacz.edu.gson;

import cn.hacz.edu.entity.PersonEntity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.Test;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/8
 * Time：17:32
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
public class Gson01MainTest {

    PersonEntity person01 = new PersonEntity("guod", 22, "18838177689");
    PersonEntity person02 = new PersonEntity("guod", 22, "18838177689");

    @Test
    public void gson01MainTest01() {
        Gson gson = new Gson();
        // 序列化
        String s = gson.toJson(person01);
        System.out.println(s);
        // 反序列化
        PersonEntity personEntity = gson.fromJson(s, PersonEntity.class);
        System.out.println(personEntity);
    }

    @Test
    public void gson01MainTest02() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        // 序列化
        String s = gsonBuilder.create().toJson(person01);
        System.out.println(s);
        // 反序列化
        PersonEntity personEntity = gsonBuilder.create().fromJson(s, PersonEntity.class);
        System.out.println(personEntity);
    }
}
