package cn.hacz.edu.xml;

import cn.hacz.edu.xmlvo.Root;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.Test;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/8
 * Time：17:51
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
public class Xml01MainTest {
    @Test
    public void Xml01MainTest01() throws JsonProcessingException {
        Root root = new Root();
        ObjectWriter objectWriter = new XmlMapper().writerWithDefaultPrettyPrinter();
        String s = objectWriter.writeValueAsString(root);
        System.out.println(s);
    }
}
