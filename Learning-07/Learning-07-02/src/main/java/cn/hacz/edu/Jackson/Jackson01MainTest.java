package cn.hacz.edu.Jackson;

import cn.hacz.edu.entity.PersonEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/8
 * Time：17:19
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
public class Jackson01MainTest {
    ObjectMapper objectMapper = new ObjectMapper();
    PersonEntity person01 = new PersonEntity("guod", 22, "18838177689");
    PersonEntity person02 = new PersonEntity("guod", 22, "18838177689");
    @Test
    public void jackson01MainTest01() throws IOException {
        // 序列化
        String s = objectMapper.writeValueAsString(person01);
        System.out.println(s);
        // 反序列化（两种方式）
        PersonEntity personEntity = objectMapper.readValue(s, PersonEntity.class);
        System.out.println(personEntity.getAge());
    }
}
