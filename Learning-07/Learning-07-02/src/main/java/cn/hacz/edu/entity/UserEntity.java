package cn.hacz.edu.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/8
 * Time：15:18
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Log4j
public class UserEntity {
    private String name;
    private int age;
}
