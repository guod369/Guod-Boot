package cn.hacz.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/8
 * Time：12:58
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@SpringBootApplication
@ServletComponentScan
public class Learning0604Application {
    public static void main(String[] args) {
        SpringApplication.run(Learning0604Application.class, args);
    }
}
