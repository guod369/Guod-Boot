package cn.hacz.edu.controller;

import cn.hacz.edu.webexception.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/8
 * Time：11:56
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@RestController
public class IndexController {
    @GetMapping(value = "/index")
    public R index() {
        return R.ok();
    }
}
