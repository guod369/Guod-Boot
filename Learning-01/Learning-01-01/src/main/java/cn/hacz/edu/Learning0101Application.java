package cn.hacz.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * project -
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/2/7 时间:8:54
 * @JDK JDK1.8
 * @Description 功能模块：
 */
@SpringBootApplication
public class Learning0101Application {
    public static void main(String[] args) {
        SpringApplication.run(Learning0101Application.class, args);
    }
}
