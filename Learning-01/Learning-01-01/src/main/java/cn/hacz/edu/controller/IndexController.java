package cn.hacz.edu.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project - 统一资源Aop切面定义
 * description - 根据自定义注解配置自动设置配置的资源类型到指定的字段
 *
 * @author：guod
 * ===============================
 * Created with IDEA
 * Date：2018/2/7
 * Time：9:03
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@RestController
public class IndexController {
    @RequestMapping(value = "/index")
    public String index() {
        return "hello world";
    }
}
