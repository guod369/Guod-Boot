package cn.hacz.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * project - 统一资源Aop切面定义
 * description - 根据自定义注解配置自动设置配置的资源类型到指定的字段
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/7
 * Time：10:18
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@SpringBootApplication
public class Learning0103Application {
    public static void main(String[] args) {
        SpringApplication.run(Learning0103Application.class, args);
    }
}
