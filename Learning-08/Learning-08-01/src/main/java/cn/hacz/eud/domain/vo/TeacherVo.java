package cn.hacz.eud.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * project -
 * description -
 *
 * @author：guod
 * ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：14:41
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeacherVo {
    /**
     * 分组验证接口
     */
    public interface GroupValidator {
    }

    @NotNull(message = "manufacturer不能为空", groups = GroupValidator.class)
    private String manufacturer;

    @NotNull(message = "licensePlate不能为空")
    @Size(min = 2, max = 14)
    private String licensePlate;

    @Min(value = 18, message = "seatCount不能小于18", groups = GroupValidator.class)
    @Max(value = 25, message = "seatCount不能大于25")
    private int seatCount;
}
