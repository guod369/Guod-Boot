package cn.hacz.eud.controller;

import cn.hacz.eud.domain.vo.TeacherVo;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：14:43
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@RestController
public class TeacherController {
    /**
     * 功能：单独校验
     *
     * @param teacherVo
     * @param bindingResult
     * @return
     */
    @GetMapping(value = "/index")
    public String index(@Validated TeacherVo teacherVo, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getFieldError().getDefaultMessage());
        }
        return teacherVo.getLicensePlate();
    }

    /**
     * 功能：分组校验
     *
     * @param teacherVo
     * @param bindingResult
     * @return
     */
    @GetMapping(value = "/indexGroup")
    public String indexGroup(@Validated({TeacherVo.GroupValidator.class}) TeacherVo teacherVo, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult.getFieldError().getDefaultMessage());
        }
        return teacherVo.getLicensePlate();
    }
}
