package cn.hacz.eud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：14:40
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@SpringBootApplication
public class Learning0801Application {
    public static void main(String[] args) {
        SpringApplication.run(Learning0801Application.class, args);
    }
}
