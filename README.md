### spring boot 企业开发

##### Learning-01 点睛spring
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-01-01        |  Spring基础      |  简书文档资料  |[官网]()|
| Learning-01-02        |  Spring常用配置   |  简书文档资料  |[官网]()|
| Learning-01-03        |  Spring高级配置   |  简书文档资料  |[官网]()|


##### Learning-02 点睛spring MVC
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-01-01        |  Spring基础      |  简书文档资料  |[官网]()|
| Learning-01-02        |  Spring常用配置   |  简书文档资料  |[官网]()|
| Learning-01-03        |  Spring高级配置   |  简书文档资料  |[官网]()|


##### Learning-03 实战spring boot
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-03-01        |  Spring boot简单项目的快速搭建      |  简书文档资料  |[官网]()|
| Learning-03-02        |  Spring boot配置文件详解：Properties   |  简书文档资料  |[官网]()|
| Learning-03-03        |  Spring boot配置文件详解：YAML   |  简书文档资料  |[官网]()|
| Learning-03-04        |  Spring boot配置文件-多环境配置   |  简书文档资料  |[官网]()|
| Learning-03-05        |  Spring boot配置文件-优雅的属性配置   |  简书文档资料  |[官网]()|
| Learning-03-06        |  Spring boot域对象-VO、DTO、DO、PO   |  简书文档资料  |[官网]()|
| Learning-03-07        |  Spring boot工具类Java8中的LocalDateTime   |  简书文档资料  |[官网]()|



##### Learning-04 实战spring boot的WEB应用视图开发
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-04-01        |  模板引擎FreeMarker      |  简书文档资料  |[官网]()|
| Learning-04-02        |  模板引擎Thymeleaf   |  简书文档资料  |[官网]()|
| Learning-04-03        |  模板引擎JSP   |  简书文档资料  |[官网]()|


##### Learning-05 log日志配置
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-05-01        |  日志配置-logback      |  简书文档资料  |[官网]()|
| Learning-05-02        |  日志配置-log4j2   |  简书文档资料  |[官网]()|
| Learning-05-03        |  待定   |  简书文档资料  |[官网]()|



##### Learning-06 WEB应用开发基础操作
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-06-01        |  统一异常处理      |  简书文档资料  |[官网]()|
| Learning-06-02        |  Servlets, Filters, listeners   |  简书文档资料  |[官网]()|
| Learning-06-03        |  CORS支持跨域请求处理   |  简书文档资料  |[官网]()|
| Learning-06-04        |  文件上传   |  简书文档资料  |[官网]()|
| Learning-06-05        |  SpringMvcConfig   |  简书文档资料  |[官网]()|
| Learning-06-06        |  邮件发送   |  简书文档资料  |[官网]()|
| Learning-06-07        |  发送邮件-使用模板邮件并实现多账号轮询发送   |  简书文档资料  |[官网]()|

##### Learning-07 简洁的JSON和XML数据交互
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-07-01        |  Json、JavaBean、Xml互转的几种工具介绍+fastjson      |  简书文档资料  |[官网]()|
| Learning-07-02        |  Json、JavaBean、Xml互转的几种工具介绍+Jackson   |  简书文档资料  |[官网]()|
| Learning-07-03        |  Json、JavaBean、Xml互转的几种工具介绍+Google的Gson   |  简书文档资料  |[官网]()|
| Learning-07-04        |  Json、JavaBean、Xml互转的几种工具介绍+Json-lib   |  简书文档资料  |[官网]()|

##### Learning-08 让传输数据更安全
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-08-01        |  Hibernate Validator      |  简书文档资料  |[官网]()|
| Learning-08-02        |  待定   |  简书文档资料  |[官网]()|
| Learning-08-03        |  待定   |  简书文档资料  |[官网]()|
| Learning-08-04        |  待定   |  简书文档资料  |[官网]()|


##### Learning-09 Rest full接口
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-09-01        |  Rest Full的常用方式      |  简书文档资料  |[官网]()|
| Learning-09-02        |  待定   |  简书文档资料  |[官网]()|
| Learning-09-03        |  待定   |  简书文档资料  |[官网]()|
| Learning-09-04        |  待定   |  简书文档资料  |[官网]()|

##### Learning-10 SQL关系型数据库
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-10-01        |  SQL关系型数据库-JDBC      |  简书文档资料  |[官网]()|
| Learning-10-02        |  SQL关系型数据库-JDBCTemplate      |  简书文档资料  |[官网]()|
| Learning-10-03        |  SQL关系型数据库-Spring-data-JPA   |  简书文档资料  |[官网]()|
| Learning-10-04        |  SQL关系型数据库-Hibernate   |  简书文档资料  |[官网]()|
| Learning-10-05        |  SQL关系型数据库-Mybatis-XML   |  简书文档资料  |[官网]()|
| Learning-10-06        |  SQL关系型数据库-Mybatis-Annotation   |  简书文档资料  |[官网]()|
| Learning-10-07        |  SQL关系型数据库-Mybatis-Plus   |  简书文档资料  |[官网]()|
| Learning-10-08        |  SQL关系型数据库-QueryDSL   |  简书文档资料  |[官网]()|
| Learning-10-09        |  SQL关系型数据库-事务处理   |  简书文档资料  |[官网]()|
| Learning-10-10        |  SQL关系型数据库-H2嵌入式数据库的使用   |  简书文档资料  |[官网]()|

##### Learning-11 NoSQL非关系型数据库
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-11-01        |  NoSQL数据库-Redis      |  简书文档资料  |[官网]()|
| Learning-11-02        |  NoSQL数据库-Mongodb   |  简书文档资料  |[官网]()|
| Learning-11-03        |  Caching-EhCache   |  简书文档资料  |[官网]()|
| Learning-11-04        |  Caching-Redis   |  简书文档资料  |[官网]()|

##### Learning-12 完成定时任务
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-12-01        |  待定      |  简书文档资料  |[官网]()|
| Learning-12-02        |  待定   |  简书文档资料  |[官网]()|
| Learning-12-03        |  待定   |  简书文档资料  |[官网]()|
| Learning-12-04        |  待定   |  简书文档资料  |[官网]()|


##### Learning-13 异步消息服务
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-13-01        |  异步消息服务-JMS（ActiveMQ）      |  简书文档资料  |[官网]()|
| Learning-13-02        |  使用异步消息服务-AMQP（RabbitMQ）   |  简书文档资料  |[官网]()|
| Learning-13-03        |  调用REST服务-如何使用代理   |  简书文档资料  |[官网]()|
| Learning-13-04        |  待定   |  简书文档资料  |[官网]()|


##### Learning-14 WebSocket通信
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-14-01        |  WebSocket配置广播式通信      |  简书文档资料  |[官网]()|
| Learning-14-02        |  待定   |  简书文档资料  |[官网]()|
| Learning-14-03        |  待定   |  简书文档资料  |[官网]()|
| Learning-14-04        |  待定   |  简书文档资料  |[官网]()|

##### Learning-15 安全权限管理服务
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-15-01        |  待定      |  简书文档资料  |[官网]()|
| Learning-15-02        |  待定   |  简书文档资料  |[官网]()|
| Learning-15-03        |  待定   |  简书文档资料  |[官网]()|
| Learning-15-04        |  待定   |  简书文档资料  |[官网]()|


##### Learning-16 Activiti5工作流程
| 项目        | 备注   |  描述  | 官网地址  |
| :----:   | :-----  | :----:  |:----:  |
| Learning-16-01        |  Activiti5工作流程入门      |  简书文档资料  |[官网]()|
| Learning-16-02        |  待定   |  简书文档资料  |[官网]()|
| Learning-16-03        |  待定   |  简书文档资料  |[官网]()|
| Learning-16-04        |  待定   |  简书文档资料  |[官网]()|