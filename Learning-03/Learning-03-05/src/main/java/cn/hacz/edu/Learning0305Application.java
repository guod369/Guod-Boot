package cn.hacz.edu;

import cn.hacz.edu.properties.UserProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * project - ETC发票系统
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/3/15 时间:17:38
 * @JDK 1.8
 * @Description 功能模块：
 */
@SpringBootApplication
@RestController
public class Learning0305Application {
    public static void main(String[] args) {
        SpringApplication.run(Learning0305Application.class, args);
    }

    @Autowired
    private UserProperties userProperties;
    @Value("${guod.info.value.name}")
    private String name;
    @Value("${guod.info.value.age}")
    private String age;

    @GetMapping(value = "/index")
    public String index() {
        return userProperties.getName() + userProperties.getAge();
    }

    @GetMapping(value = "/value")
    public String value() {
        return name + age;
    }
}
