package cn.hacz.edu;

import cn.hacz.edu.util.QRCodeUtil;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

/**
 * project - ETC发票系统
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/3/31 时间:10:03
 * @JDK 1.8
 * @Description 功能模块：
 */
@SpringBootApplication
@RestController
public class Learning0307Application {
    public static void main(String[] args) {
        SpringApplication.run(Learning0307Application.class, args);
    }

    @RequestMapping(value = "/qrcode")
    public void qrcode(String content, @RequestParam(defaultValue = "300", required = false) int width, @RequestParam(defaultValue = "300", required = false) int height, HttpServletResponse response) {
        ServletOutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();
            QRCodeUtil.writeToStream(content, outputStream, width, height);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @RequestMapping(value = "/test")
    public void test(String content, String logoUrl, @RequestParam(defaultValue = "300") int width, @RequestParam(defaultValue = "300") int height, HttpServletResponse response) {
        ServletOutputStream outputStream = null;
        try {
            outputStream = response.getOutputStream();
            // 根据 QRCodeUtil.toBufferedImage() 返回的 BufferedImage 创建图片构件对象
            Thumbnails.Builder<BufferedImage> builder = Thumbnails.of(QRCodeUtil.toBufferedImage(content, width, height));
            // 将 logo 的尺寸设置为二维码的 30% 大小, 可以自己根据需求调节
            BufferedImage logoImage = Thumbnails.of(new URL(logoUrl)).forceSize((int) (width * 0.3), (int) (height * 0.3)).asBufferedImage();
            // 设置水印位置(居中), 水印图片 BufferedImage, 不透明度(1F 代表不透明)
            builder.watermark(Positions.CENTER, logoImage, 1F).scale(1F);
            // 此处需指定图片格式, 否者报 Output format not specified 错误
            builder.outputFormat("png").toOutputStream(outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


}
