import cn.hacz.edu.util.LocalDateTimeUtils;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 * project - ETC发票系统
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/3/31 时间:10:12
 * @JDK 1.8
 * @Description 功能模块：
 */
public class LocaDateTimeUtilsTest {
    @Test
    public void date() {
        // 初始化数据
        Date date = new Date();
        LocalDateTime localTime1 = LocalDateTime.now();
        LocalTime localTime2 = LocalTime.of(21, 30, 59, 11001);
        System.out.println(date + "\n" + localTime1 + "\n" + localTime2);
        // 相互转换
        LocalDateTime localDateTime = LocalDateTimeUtils.convertDateToLDT(date);
        System.out.println(localDateTime);
    }

    @Test
    public void format_test() {
        System.out.println(LocalDateTimeUtils.formatNow("yyyy年MM月dd日 HH:mm:ss"));
    }

    @Test
    public void betweenTwoTime_test() {
        LocalDateTime start = LocalDateTime.of(1993, 10, 13, 11, 11);
        LocalDateTime end = LocalDateTime.of(1994, 11, 13, 13, 13);
        System.out.println("年:" + LocalDateTimeUtils.betweenTwoTime(start, end, ChronoUnit.YEARS));
        System.out.println("月:" + LocalDateTimeUtils.betweenTwoTime(start, end, ChronoUnit.MONTHS));
        System.out.println("日:" + LocalDateTimeUtils.betweenTwoTime(start, end, ChronoUnit.DAYS));
        System.out.println("半日:" + LocalDateTimeUtils.betweenTwoTime(start, end, ChronoUnit.HALF_DAYS));
        System.out.println("小时:" + LocalDateTimeUtils.betweenTwoTime(start, end, ChronoUnit.HOURS));
        System.out.println("分钟:" + LocalDateTimeUtils.betweenTwoTime(start, end, ChronoUnit.MINUTES));
        System.out.println("秒:" + LocalDateTimeUtils.betweenTwoTime(start, end, ChronoUnit.SECONDS));
        System.out.println("毫秒:" + LocalDateTimeUtils.betweenTwoTime(start, end, ChronoUnit.MILLIS));
    }

    @Test
    public void plus_test() {
        //增加二十分钟
        System.out.println(LocalDateTimeUtils.formatTime(LocalDateTimeUtils.plus(LocalDateTime.now(), 20, ChronoUnit.MINUTES), "yyyy年MM月dd日 HH:mm"));
        //增加两年
        System.out.println(LocalDateTimeUtils.formatTime(LocalDateTimeUtils.plus(LocalDateTime.now(), 2, ChronoUnit.YEARS), "yyyy年MM月dd日 HH:mm"));
    }

    @Test
    public void dayStart_test() {
        // 获取一天的开始
        System.out.println(LocalDateTimeUtils.getDayStart(LocalDateTime.now()));
        // 获取一天的结束
        System.out.println(LocalDateTimeUtils.getDayEnd(LocalDateTime.now()));
    }
}
