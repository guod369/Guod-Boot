package cn.hacz.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * ========================
 * Created with IntelliJ IDEA.
 *
 * @author guod
 * Date：2018/3/14
 * Time：18:36
 * Description：功能模块：
 * JDK：V1.8
 * GitHub地址：https://github.com/guod369
 * ========================
 */
@SpringBootApplication
public class Learning0304Application {
    public static void main(String[] args) {
        SpringApplication.run(Learning0304Application.class, args);
    }
}
