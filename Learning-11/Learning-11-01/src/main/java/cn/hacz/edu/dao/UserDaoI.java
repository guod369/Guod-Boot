package cn.hacz.edu.dao;

import cn.hacz.edu.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：15:20
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
public interface UserDaoI extends JpaRepository<UserEntity, Long> {
}
