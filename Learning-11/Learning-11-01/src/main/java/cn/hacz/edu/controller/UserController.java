package cn.hacz.edu.controller;

import cn.hacz.edu.entity.UserEntity;
import cn.hacz.edu.service.UserServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：15:25
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@RestController
public class UserController {
    @Autowired
    private UserServiceI userServiceI;

    @GetMapping(value = "/index")
    public void index() {
        List<UserEntity> listUser = userServiceI.getListUser();
        System.out.println(listUser);
    }
}
