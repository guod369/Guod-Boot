package cn.hacz.edu.service;

import cn.hacz.edu.entity.UserEntity;

import java.util.List;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：15:23
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
public interface UserServiceI {
    List<UserEntity> getListUser();
}
