package cn.hacz.edu.dao;

import cn.hacz.edu.entity.UserEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.io.Serializable;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：15:59
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
public interface UserDaoI extends MongoRepository<UserEntity, Serializable> {
    UserEntity findByName(String name);
}
