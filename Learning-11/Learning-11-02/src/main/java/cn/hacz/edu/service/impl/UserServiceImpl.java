package cn.hacz.edu.service.impl;

import cn.hacz.edu.dao.UserDaoI;
import cn.hacz.edu.entity.UserEntity;
import cn.hacz.edu.service.UserServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：16:01
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@Service
public class UserServiceImpl implements UserServiceI {
    @Autowired
    private UserDaoI userDaoI;

    @Override
    public void save(UserEntity user) {
        userDaoI.save(user);
    }

    @Override
    public UserEntity findByName(String name) {
        return this.userDaoI.findByName(name);
    }
}
