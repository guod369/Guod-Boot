package cn.hacz.edu.service;

import cn.hacz.edu.entity.UserEntity;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：16:00
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
public interface UserServiceI {
    void save(UserEntity user);

    UserEntity findByName(String name);
}
