package cn.hacz.edu.service;

import cn.hacz.edu.entity.tree.TreeVo;
import cn.hacz.edu.utils.tree.MenuTree;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/11
 * Time：10:22
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class TreeMainTest {
    @Autowired
    private TreeVoServiceI treeVoServiceI;

    @Test
    public void doSaveTest() {
        TreeVo treeVo00 = new TreeVo();
        treeVo00.setName("父亲");

        TreeVo treeVo01 = new TreeVo();
        treeVo01.setName("父亲大孩子");
        TreeVo treeVo02 = new TreeVo();
        treeVo02.setName("父亲二孩子");

        TreeVo treeVo0101 = new TreeVo();
        treeVo0101.setName("父亲大孩子的儿子");
        TreeVo treeVo010101 = new TreeVo();
        treeVo010101.setName("父亲大孩子的儿子的儿子");


        TreeVo treeVo0102 = new TreeVo();
        treeVo0102.setName("父亲二孩子的儿子");
        TreeVo treeVo010201 = new TreeVo();
        treeVo010201.setName("父亲二孩子的儿子的儿子");

        treeVo00.getChildren().add(treeVo01);
        treeVo00.getChildren().add(treeVo02);
        treeVo01.getChildren().add(treeVo0101);
        treeVo0101.getChildren().add(treeVo010101);
        treeVo02.getChildren().add(treeVo0102);
        treeVo0102.getChildren().add(treeVo010201);

        treeVo01.setParent(treeVo00);
        treeVo02.setParent(treeVo00);
        treeVo0101.setParent(treeVo01);
        treeVo0102.setParent(treeVo02);
        treeVo010101.setParent(treeVo0101);
        treeVo010201.setParent(treeVo0102);

        treeVoServiceI.doSave(treeVo00);
    }

    @Test
    public void doFindAllTest() {
        List<TreeVo> allTree = treeVoServiceI.getAllTree();
        MenuTree menuTree = new MenuTree();
        List<Object> objects = menuTree.menuList(allTree);
        String s = JSON.toJSONString(objects);
        System.out.println(s);
    }

}
