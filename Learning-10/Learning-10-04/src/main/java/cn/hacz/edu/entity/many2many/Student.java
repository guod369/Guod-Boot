package cn.hacz.edu.entity.many2many;

import cn.hacz.edu.entity.base.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * @author guod
 */
@Entity
@Table(name = "tb_student")
public class Student extends BaseEntity {
    private String name;
    private Integer age;
    @ManyToMany(mappedBy = "student")
    private Set<Teacher> teacher = new HashSet<>();

    public Integer getAge() {
        return age;
    }

    public String getNaem() {
        return name;
    }

    public Set<Teacher> getTeacher() {
        return teacher;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setNaem(String name) {
        this.name = name;
    }

    public void setTeacher(Set<Teacher> teacher) {
        this.teacher = teacher;
    }

}
