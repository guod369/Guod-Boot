package cn.hacz.edu.dao;

import cn.hacz.edu.entity.tree.TreeVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/11
 * Time：9:53
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@Component
public interface TreeVoI extends JpaRepository<TreeVo, Serializable> {
}
