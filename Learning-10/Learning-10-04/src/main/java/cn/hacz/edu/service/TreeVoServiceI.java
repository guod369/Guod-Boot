package cn.hacz.edu.service;

import cn.hacz.edu.entity.tree.TreeVo;

import java.util.List;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/11
 * Time：9:54
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
public interface TreeVoServiceI {
    void doSave(TreeVo treeVo);

    List<TreeVo> getAllTree();
}
