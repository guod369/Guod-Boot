package cn.hacz.edu;

import org.hibernate.SessionFactory;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * project -
 * description -
 *
 * @author：guod ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：13:07
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@SpringBootApplication
public class Learning1004Application {
    public static void main(String[] args) {
        SpringApplication.run(Learning1004Application.class, args);
    }

    /**
     * 功能：配置hibernate的SessionFactory可以全局使用
     *
     * @param hemf
     * @return
     */
    @Bean
    public SessionFactory sessionFactory(HibernateEntityManagerFactory hemf) {
        return hemf.getSessionFactory();
    }
}
