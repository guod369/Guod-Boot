package cn.hacz.edu.entity.one2many;

import cn.hacz.edu.entity.base.BaseEntity;
import cn.hacz.edu.utils.hibernate.Comment;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * @author guod
 */
@Entity
@Table(name = "tb_group")
public class Group extends BaseEntity {
    @Comment("组名称")
    private String name;
    /**
     * 在一的一端Group端配置时，在users只需要加个mappedBy="groupid"
     */
    @Comment("组用户")
    @OneToMany(mappedBy = "group")
    private Set<User> users = new HashSet<>();

    public String getName() {
        return name;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

}
