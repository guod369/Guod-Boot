package cn.hacz.edu.entity.many2many;

import cn.hacz.edu.entity.base.BaseEntity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author guod
 */
@Entity
@Table(name = "tb_teacher")
public class Teacher extends BaseEntity {
    private String naem;
    private Integer age;
    @ManyToMany
    @JoinTable(
            name = "tb_teacher_student",
            joinColumns = {@JoinColumn(name = "teacher_id")},
            inverseJoinColumns = {@JoinColumn(name = "student_id")}
    )
    private Set<Student> student = new HashSet<>();

    public Integer getAge() {
        return age;
    }

    public String getNaem() {
        return naem;
    }

    public Set<Student> getStudent() {
        return student;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setNaem(String naem) {
        this.naem = naem;
    }

    public void setStudent(Set<Student> student) {
        this.student = student;
    }

}
