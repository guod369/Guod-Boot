package cn.hacz.edu.entity.one2one;

import cn.hacz.edu.entity.base.BaseEntity;
import cn.hacz.edu.utils.hibernate.Comment;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

/**
 * @author guod
 */
@Entity
@Table(name = "tb_husband")
@DynamicInsert
@DynamicUpdate
public class Husband extends BaseEntity {
    @Comment("姓名")
    private String name;
    @Comment("年龄")
    private Integer age;
    @OneToOne(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name = "wife_id")
    private Wife wife;

    public Integer getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public Wife getWife() {
        return wife;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWife(Wife wife) {
        this.wife = wife;
    }

}
