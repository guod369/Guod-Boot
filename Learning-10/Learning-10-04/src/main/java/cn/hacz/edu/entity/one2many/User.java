package cn.hacz.edu.entity.one2many;

import cn.hacz.edu.entity.base.BaseEntity;

import javax.persistence.*;

/**
 * @author guod
 */
@Entity
@Table(name = "tb_user")
public class User extends BaseEntity {
    private String name;
    /**
     * 配置规则:一般以多的一端为主,先配置多的一端。
     * cascade的作用：设定cascade以设定在持久化时对于关联对象的操作（CUD，R归Fetch管）,Cascade的属性是数组格式。
     * cascade仅仅是帮我们省了编程的麻烦而已，不要把它的作用看的太大。
     */
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "group_id")
    private Group group;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
