package cn.hacz.edu.entity.purview;

import cn.hacz.edu.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/11
 * Time：11:29
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@Entity
@Table(name = "sys_role")
@DynamicUpdate
@DynamicInsert
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleEntity extends BaseEntity {
    /**
     * 上级角色
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PID")
    private RoleEntity role;

    /**
     * 名称
     */
    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    /**
     * 备注
     */
    @Column(name = "REMARK", length = 200)
    private String remark;

    /**
     * 排序
     */
    @Column(name = "SEQ", length = 10)
    private Integer seq;

    /**
     * 子角色
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "role")
    private Set<RoleEntity> roles = new HashSet<>(0);

    /**
     * 哪些用户有这个角色
     */
    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    private Set<UserEntity> users = new HashSet<>(0);

    /**
     * 这个角色有哪些资源
     */
    @ManyToMany
    @JoinTable(
            name = "sys_role_resource",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "resource_id")
    )
    private Set<ResourceEntity> resources = new HashSet<>(0);
}
