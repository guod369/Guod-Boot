package cn.hacz.edu.entity.purview;

import cn.hacz.edu.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/11
 * Time：11:28
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@Entity
@Table(name = "sys_user")
@DynamicUpdate
@DynamicInsert
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity extends BaseEntity {
    /**
     * 用户真实姓名
     */
    @Column(length = 20)
    private String username;
    /**
     * 身份证号
     */
    @Column(unique = true, nullable = true, length = 18)
    private String idNumber;

    /**
     * 邮箱
     */
    @Column(length = 30)
    private String email;

    /**
     * 手机号
     */
    @Column(unique = true, nullable = true, length = 11)
    private String mobilePhone;

    /**
     * 头像
     */
    @Column(length = 200)
    private String userIcon;
    /**
     * 密码
     */
    @Column(length = 30)
    private String pwd;
    /**
     * 所在公司
     */

    @Column(length = 100)
    private String company;

    /**
     * 职业
     */
    @Column(length = 50)
    private String profession;

    /**
     * 家乡地址
     */
    @Column(length = 80)
    private String homeAddress;

    /**
     * 用户有哪些角色
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(
            name = "sys_user_role",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")}
    )
    private Set<RoleEntity> roles = new HashSet<>(0);
}
