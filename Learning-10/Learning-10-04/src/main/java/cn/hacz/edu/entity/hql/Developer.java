package cn.hacz.edu.entity.hql;

import cn.hacz.edu.entity.base.BaseEntity;
import cn.hacz.edu.utils.hibernate.Comment;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "tb_hql_developer")
public class Developer extends BaseEntity {
	@Comment("姓名")
	private String name;

	@Comment("年龄")
	private Integer age;

	public Developer() {
		super();
	}

	public Developer(String name, Integer age) {
		super();
		this.name = name;
		this.age = age;
	}

	public Integer getAge() {
		return age;
	}

	public String getName() {
		return name;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public void setName(String name) {
		this.name = name;
	}

}
