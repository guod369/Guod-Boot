package cn.hacz.edu.vo;

/**
 * Json响应基类
 * @author Alan
 *
 */
public class Json {

	private boolean success;
	private String message;

	
	private Object obj;
	
	public Json(){}
	
	public Json(boolean success) {
		super();
		this.success = success;
	}

	public Json(boolean success, String message, Object obj) {
		super();
		this.success = success;
		this.message = message;
		this.obj = obj;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}






}
