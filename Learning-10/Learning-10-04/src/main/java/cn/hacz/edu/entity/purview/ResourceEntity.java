package cn.hacz.edu.entity.purview;

import cn.hacz.edu.entity.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/11
 * Time：11:30
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@Entity
@Table(name = "sys_resource")
@DynamicUpdate
@DynamicInsert
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResourceEntity extends BaseEntity {
    /**
     * 类型 ：菜单，功能
     */
    @Enumerated(EnumType.ORDINAL)
    private TypeEnum type;

    /**
     * 所属资源
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PID")
    private ResourceEntity resource;

    /**
     * 名称
     */
    @Column(name = "NAME", nullable = false, length = 100)
    private String name;

    /**
     * 备注
     */
    @Column(name = "REMARK", length = 200)
    private String remark;

    /**
     * 排序
     */
    @Column(name = "SEQ", length = 10)
    private Integer seq;

    /**
     * 地址
     */
    @Column(name = "URL", length = 200)
    private String url;

    /**
     * 图标
     */
    @Column(name = "ICON", length = 100)
    private String icon;

    /**
     * 所属角色
     */
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "resources")
    private Set<RoleEntity> roles = new HashSet<>(0);

    /**
     * 下级资源
     */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "resource")
    private Set<ResourceEntity> resources = new HashSet<>(0);

    public static enum TypeEnum {
        菜单, 功能;
    }
}
