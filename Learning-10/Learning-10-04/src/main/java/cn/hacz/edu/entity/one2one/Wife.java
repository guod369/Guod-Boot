package cn.hacz.edu.entity.one2one;

import cn.hacz.edu.entity.base.BaseEntity;

import javax.persistence.*;

/**
 * @author guod
 */
@Entity
@Table(name = "tb_wife")
public class Wife extends BaseEntity {

    private String name;

    private Integer age;

    /**
     * 注意：凡是双向关联，必设mappedBy。
     * 作用：指定这个一对一关联是被Husband类的wife属性(准确说是getWife方法)做的映射。
     */
    @OneToOne(mappedBy = "wife", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Husband husband;

    public Integer getAge() {
        return age;
    }

    public Husband getHusband() {
        return husband;
    }

    public String getName() {
        return name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setHusband(Husband husband) {
        this.husband = husband;
    }

    public void setName(String name) {
        this.name = name;
    }

}
