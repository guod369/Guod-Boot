package cn.hacz.edu.service.impl;

import cn.hacz.edu.dao.TreeVoI;
import cn.hacz.edu.entity.tree.TreeVo;
import cn.hacz.edu.service.TreeVoServiceI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/11
 * Time：9:54
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@Service
public class TreeVoServiceImpl implements TreeVoServiceI {
    @Autowired
    private TreeVoI treeVoI;
    @Override
    public void doSave(TreeVo treeVo) {
        treeVoI.save(treeVo);
    }

    @Override
    public List<TreeVo> getAllTree() {
        return treeVoI.findAll();
    }
}
