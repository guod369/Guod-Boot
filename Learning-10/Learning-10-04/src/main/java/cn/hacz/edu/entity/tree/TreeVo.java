package cn.hacz.edu.entity.tree;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * project -
 * description - 一对多和多对一的双向关联关系
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/11
 * Time：9:23
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@Entity
@Table(name = "tb_tree")
public class TreeVo implements Serializable {
    /**
     * 标识符
     */
    @Id
    @GeneratedValue
    private int id;
    private String name;
    /**
     * 层次，为了输出设计
     */
    private int level;
    /**
     * 是否为叶子节点，这是为了效率设计，可有可无
     */
    private boolean leaf;
    private String text;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    /**
     * 父节点：因为多个节点属于一个父节点，因此用hibernate映射关系说是“多对一”
     */
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "parent_id")
    private TreeVo parent;
    /**
     * 子节点：因为一个节点有多个子节点，因此用hibernate映射关系说是“一对多”
     */
    @OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<TreeVo> children = new HashSet<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TreeVo getParent() {
        return parent;
    }

    public void setParent(TreeVo parent) {
        this.parent = parent;
    }

    public Set<TreeVo> getChildren() {
        return children;
    }

    public void setChildren(Set<TreeVo> children) {
        this.children = children;
    }
}
