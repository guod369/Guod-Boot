package cn.hacz.edu.entity.base;

import cn.hacz.edu.utils.hibernate.Comment;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

/**
 * 继承类
 *
 * @author kevin
 * @date 2016-09-26 12:05:26
 */
@MappedSuperclass
public class BaseEntity {
    @Id
    @Column(name = "id", unique = true, nullable = false, length = 36)
    @Comment("uuid")
    private String id;
    /**
     * 创建时间
     */
    @Column(name = "createdatetime")
    private LocalDateTime createDateTime;
    /**
     * 修改时间
     */
    @Column(name = "modifydatetime")
    private LocalDateTime modifyDateTime;
    /**
     * 是否删除
     */
    @Type(type = "true_false")
    @Column(name = "deleted")
    private Boolean deleted;

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDateTime getModifyDateTime() {
        return modifyDateTime;
    }

    public void setModifyDateTime(LocalDateTime modifyDateTime) {
        this.modifyDateTime = modifyDateTime;
    }


}
