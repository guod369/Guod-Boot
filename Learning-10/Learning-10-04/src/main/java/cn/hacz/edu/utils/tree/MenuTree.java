package cn.hacz.edu.utils.tree;

import cn.hacz.edu.entity.tree.TreeVo;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/11
 * Time：11:10
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
public class MenuTree {
    public static Map<String, Object> mapArray = new LinkedHashMap<>();
    public List<TreeVo> menuCommon;
    public List<Object> list = new ArrayList<>();

    public List<Object> menuList(List<TreeVo> menu) {
        this.menuCommon = menu;
        for (TreeVo x : menu) {
            Map<String, Object> mapArr = new LinkedHashMap<>();
            if (x.getParent() == null) {
                mapArr.put("id", x.getId());
                mapArr.put("name", x.getName());
                mapArr.put("level", x.getLevel());
                mapArr.put("pid", x.getParent());
                mapArr.put("child", menuChild(x.getId()));
                list.add(mapArr);
            }
        }
        return list;
    }

    public List<?> menuChild(Integer id) {
        List<Object> lists = new ArrayList<>();
        for (TreeVo a : menuCommon) {
            Map<String, Object> childArray = new LinkedHashMap<>();
            // 如果是父节点的话，此处不能获取到parent的id
            if (a.getParent() == null) {
                continue;
            }
            if (a.getParent().getId() == id) {
                childArray.put("id", a.getId());
                childArray.put("name", a.getName());
                childArray.put("level", a.getLevel());
                childArray.put("pid", a.getParent().getId());
                childArray.put("child", menuChild(a.getId()));
                lists.add(childArray);
            }
        }
        return lists;
    }
}
