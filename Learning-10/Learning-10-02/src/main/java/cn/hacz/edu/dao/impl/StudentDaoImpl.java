package cn.hacz.edu.dao.impl;

import cn.hacz.edu.dao.StudentDaoI;
import cn.hacz.edu.util.JdbcDaoImpl;
import org.springframework.stereotype.Repository;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：11:46
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@Repository
public class StudentDaoImpl extends JdbcDaoImpl implements StudentDaoI {

}
