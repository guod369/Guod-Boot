package cn.hacz.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * project -
 * description -
 *
 * @author：guod <br/>
 * ===============================
 * Created with IDEA
 * Date：2018/2/9
 * Time：11:33
 * JDK: 1.8
 * Version:1.0
 * ================================
 */
@SpringBootApplication
public class Learning1002Application {
    public static void main(String[] args) {
        SpringApplication.run(Learning1002Application.class, args);
    }
}
