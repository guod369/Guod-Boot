package cn.hacz.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * project - ETC发票系统
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/4/29 时间:8:29
 * @JDK 1.8
 * @Description 功能模块：
 */
@SpringBootApplication
public class Learning1001Application {
    public static void main(String[] args) {
        SpringApplication.run(Learning1001Application.class, args);
    }
}
