package cn.hacz.edu.dao;

import cn.hacz.edu.domain.one2one.WifeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

/**
 * project - ETC发票系统
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/4/29 时间:8:51
 * @JDK 1.8
 * @Description 功能模块：
 */
@Repository
public interface WifeDaoI extends JpaRepository<WifeEntity, Serializable> {
}
