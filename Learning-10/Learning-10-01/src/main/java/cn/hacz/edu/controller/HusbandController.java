package cn.hacz.edu.controller;

import cn.hacz.edu.dao.HusbandDaoI;
import cn.hacz.edu.dao.WifeDaoI;
import cn.hacz.edu.domain.one2one.HusbandEntity;
import cn.hacz.edu.domain.one2one.WifeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * project - ETC发票系统
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/4/29 时间:8:55
 * @JDK 1.8
 * @Description 功能模块：
 */
@RestController
public class HusbandController {
    @Autowired
    private HusbandDaoI husbandDaoI;
    @Autowired
    private WifeDaoI wifeDaoI;

    @PostMapping(value = "/doSaveHusband")
    public void doSaveHusband() {
        HusbandEntity husbandEntity = new HusbandEntity();
        husbandEntity.setName("guo");
        WifeEntity wifeEntity = new WifeEntity();
        wifeEntity.setName("sun");
        husbandEntity.setWife(wifeEntity);
        wifeEntity.setHusband(husbandEntity);
        wifeDaoI.save(wifeEntity);
    }

    @DeleteMapping(value = "/doDeleteHusband/{id}")
    public void doDeleteHusband(@PathVariable(name = "id") Integer id) {
        wifeDaoI.delete(id);
    }


    @GetMapping(value = "/findHusband/{id}")
    public void findHusband(@PathVariable(name = "id") Integer id) {
        WifeEntity one = wifeDaoI.findOne(id);
        System.out.println(one.getHusband().getName());
    }


}
