package cn.hacz.edu.domain.one2one;

import cn.hacz.edu.domain.base.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * project - ETC发票系统
 *
 * @author guod
 * @version 3.0
 * @date 日期:2018/4/29 时间:8:34
 * @JDK 1.8
 * @Description 功能模块：
 */
@Entity
@Table(name = "h_wife")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WifeEntity extends BaseEntity {
    private String name;
    private Integer age;
    @OneToOne(mappedBy = "wife", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private HusbandEntity husband;
}
